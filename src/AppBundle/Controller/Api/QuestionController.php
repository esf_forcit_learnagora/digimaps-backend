<?php

namespace AppBundle\Controller\Api;


use AppBundle\Entity\Mcq;
use AppBundle\Entity\Repository\AnswerRepository;
use AppBundle\Entity\User;
use AppBundle\Entity\Option;
use AppBundle\Entity\Answer;
use AppBundle\Entity\Repository\QuestionRepository;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class QuestionController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var QuestionRepository $questionRepository
     *
     * @DI\Inject("digimaps.repository.question")
     */
    private $questionRepository;

    /**
     * @var AnswerRepository $answerRepository
     *
     * @DI\Inject("digimaps.repository.answer")
     */
    private $answerRepository;

    public function answerAction($id, $optionId)
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var MCQ $question */
        $question = $this->questionRepository->find($id);

        // check if there was already an answer
        $answer = $this->answerRepository->getAnswerFor($user,$question);

        $option = null;
        /** @var Option $o */
        foreach ($question->getOptions() as $o) {
            if ($o->getId() == $optionId) {
                $option = $o;
            }
        }

        if ($option) {
            $answer = $answer ? $answer : new Answer();
            $answer->setUser($user)->setOption($option)->setDate(new \DateTime());
            $this->entityManager->persist($answer);
            $this->entityManager->flush();
        } else {
            return View::create(['result'=>'option not found'], 404);
        }
        return View::create(['result'=>'OK'], 200);
    }




}
