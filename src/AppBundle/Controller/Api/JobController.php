<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Goal;
use AppBundle\Entity\Job;
use AppBundle\Entity\Rating;
use AppBundle\Entity\Repository\GoalRepository;
use AppBundle\Entity\Repository\RatingRepository;
use AppBundle\Entity\Repository\TaskRepository;
use AppBundle\Entity\Task;
use AppBundle\Entity\User;
use AppBundle\Model\Profile\ProfileMapper;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Repository\JobRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class JobController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var JobRepository $jobRepository
     *
     * @DI\Inject("digimaps.repository.job")
     */
    private $jobRepository;

    /**
     * @var TaskRepository $taskRepository
     *
     * @DI\Inject("digimaps.repository.task")
     */
    private $taskRepository;

    /**
     * @var RatingRepository $ratingRepository
     *
     * @DI\Inject("digimaps.repository.rating")
     */
    private $ratingRepository;

    /**
     * @var goalRepository $goalRepository
     *
     * @DI\Inject("digimaps.repository.goal")
     */
    private $goalRepository;

    /**
     * @var ProfileMapper $profileMapper
     *
     *  @DI\Inject("app.mapper.profile"),
     */
    private $profileMapper;

    public function indexAction()
    {
        $jobs = $this->jobRepository->findAll();
        return View::create(['results'=>$jobs], 200);
    }

    public function jobAction($id) {
        $job = $this->jobRepository->find($id);
        return View::create($job, 200);
    }

    public function tasksAction($id) {
        $job = $this->jobRepository->find($id);
        $allRatings = $this->ratingRepository->findBy(array('job'=>$job));

        $tasks = array();
        /** @var Task $task */
        foreach ($job->getTasks() as $task) {
            $fullRating = 0;
            /** @var Rating $ratingObject */
            foreach ($allRatings as $ratingObject) {
                if ($ratingObject->getTask()->getId() == $task->getId()) {
                    $fullRating+= $ratingObject->getRating();
                }
            }

            $tasks[] = array(
                'task' => $task,
                'expert' => array(
                    'rating' => 0,
                    'fullRating' => $fullRating
                )
            );
        }

        return View::create(['results'=>$tasks], 200);
    }

    public function skillsAction($id) {
        /** @var Job $job */
        $job = $this->jobRepository->find($id);
        $skills = $job->getSkills();
        return View::create(['results' => $skills], 200);
    }

    public function selectAction($id) {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Job $job */
        $job = $this->jobRepository->find($id);
        if (is_null($job)) {
            return View::create(null, 404);
        }

        $goal = $this->goalRepository->findOneBy(array('user'=>$user, 'job'=>$job));
        if (is_null($goal)) {
            $goal = new Goal();
            $goal->setUser($user)->setJob($job);
            $this->entityManager->persist($goal);
            $this->entityManager->flush();
        }

        return View::create(null, 200);
    }

    public function unselectAction($id) {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Job $job */
        $goal = $this->goalRepository->find($id);
        if ($goal) {
            $this->entityManager->remove($goal);
            $this->entityManager->flush();
        }

        return View::create(null, 200);
    }

}
