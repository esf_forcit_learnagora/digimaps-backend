<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Job;
use AppBundle\Entity\Rating;
use AppBundle\Entity\Repository\RatingRepository;
use AppBundle\Entity\Repository\TaskRepository;
use AppBundle\Entity\Task;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Repository\JobRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class ExpertController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var JobRepository $jobRepository
     *
     * @DI\Inject("digimaps.repository.job")
     */
    private $jobRepository;

    /**
     * @var TaskRepository $taskRepository
     *
     * @DI\Inject("digimaps.repository.task")
     */
    private $taskRepository;

    /**
     * @var RatingRepository $ratingRepository
     *
     * @DI\Inject("digimaps.repository.rating")
     */
    private $ratingRepository;

    public function tasksAction($id) {
        /** @var User $user */
        $user = $this->getUser();
        $job = $this->jobRepository->find($id);
        $ratings = $this->ratingRepository->findBy(array('user'=>$user,'job'=>$job));
        $allRatings = $this->ratingRepository->findBy(array('job'=>$job));

        $tasks = array();
        /** @var Task $task */
        foreach ($job->getTasks() as $task) {
            $rating = 0;
            /** @var Rating $ratingObject */
            foreach ($ratings as $ratingObject) {
                if ($ratingObject->getTask()->getId() == $task->getId()) {
                    $rating = $ratingObject->getRating();
                }
            }

            $fullRating = 0;
            /** @var Rating $ratingObject */
            foreach ($allRatings as $ratingObject) {
                if ($ratingObject->getTask()->getId() == $task->getId()) {
                    $fullRating+= $ratingObject->getRating();
                }
            }

            $tasks[] = array(
                'task' => $task,
                'expert' => array(
                    'rating' => $rating,
                    'fullRating' => $fullRating
                )
            );
        }

        return View::create(['results'=>$tasks], 200);
    }

    /**
     * @param $jobId
     * @param $taskId
     * @param $rating
     * @return static
     */
    public function rateTaskAction($jobId, $taskId, $rating) {

        /** @var User $user */
        $user = $this->getUser();

        /** @var Job $job */
        $job = $this->jobRepository->find($jobId);
        /** @var Task $task */
        $task = $this->taskRepository->find($taskId);
        /** @var Rating $ratingObject */
        $ratingObject = $this->ratingRepository->findOneBy(array('user'=>$user, 'job'=>$job, 'task'=>$task));
        if (!$ratingObject) {
            $ratingObject = new Rating();
            $ratingObject->setUser($user)->setJob($job)->setTask($task);
        }
        $ratingObject->setRating($rating);
        $this->entityManager->persist($ratingObject);
        $this->entityManager->flush();

        /** @var Rating $rating */
        $rating = $this->ratingRepository->findOneBy(array('user'=>$user,'task'=>$task));
        $allRatings = $this->ratingRepository->findBy(array('task'=>$task));
        $fullRating = 0;
        /** @var Rating $r */
        foreach ($allRatings as $r) {
            $fullRating+= $r->getRating();
        }

        $expert = array(
            'rating' => $rating->getRating(),
            'fullRating' => $fullRating
        );

        return View::create(['results'=>$expert], 200);
    }

}
