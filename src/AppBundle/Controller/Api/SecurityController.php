<?php

namespace AppBundle\Controller\Api;


use AppBundle\Entity\User;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class SecurityController extends Controller
{

    public function registerAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $data = $request->request->all();

        // Do a check for existing user with userManager->findByUsername
        $user = $userManager->findUserByUsername($data['username']);
        if ($user) {
            return View::create(['result'=>'Username is already in use'], 400);
        }

        /** @var User $user */
        $user = $userManager->createUser();
        $user->setFirstName($data["firstname"]);
        $user->setLastName($data["lastname"]);
        $user->setUsername($data['username']);
        $user->setEmail($data["email"]);
        $user->setPlainPassword($data['password']);
        $user->setEnabled(true);

        $userManager->updateUser($user, true);

        return View::create(['result'=>'OK'], 200);
    }
}
