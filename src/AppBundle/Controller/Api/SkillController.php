<?php

namespace AppBundle\Controller\Api;


use AppBundle\Entity\Answer;
use AppBundle\Entity\Repository\AnswerRepository;
use AppBundle\Entity\Repository\QuestionRepository;
use AppBundle\Entity\Repository\SkillRepository;
use AppBundle\Entity\Repository\UserSkillRepository;
use AppBundle\Entity\Skill;
use AppBundle\Entity\User;
use AppBundle\Entity\UserSkill;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class SkillController extends Controller
{
    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var SkillRepository $skillRepository
     *
     * @DI\Inject("digimaps.repository.skill")
     */
    private $skillRepository;

    /**
     * @var UserSkillRepository $userSkillRepository
     *
     * @DI\Inject("digimaps.repository.user_skill")
     */
    private $userSkillRepository;

    /**
     * @var QuestionRepository $questionRepository
     *
     * @DI\Inject("digimaps.repository.question")
     */
    private $questionRepository;

    /**
     * @var AnswerRepository $answerRepository
     *
     * @DI\Inject("digimaps.repository.answer")
     */
    private $answerRepository;

    public function indexAction()
    {
        $skills = $this->skillRepository->findAll();
        return View::create(['results'=>$skills], 200);
    }

    public function skillAction($id) {
        $skill = $this->skillRepository->find($id);
        return View::create($skill, 200);
    }

    public function questionsAction($id) {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Skill $skill */
        $skill = $this->skillRepository->find($id);
        $questions = $this->questionRepository->findForSkill($skill);

        $unansweredQuestions = array();
        foreach ($questions as $question) {
            $answer = $this->answerRepository->getAnswerFor($user, $question);
            if (is_null($answer)) {
                $unansweredQuestions[] = $question;
            }

        }
        return View::create(['questions'=>$unansweredQuestions], 200);
    }

    public function scoreAction($id) {
        /** @var User $user */
        $user = $this->getUser();

        $numberQuestionsInTest = 10;

        /** @var Skill $skill */
        $skill = $this->skillRepository->find($id);
        $answers = $this->answerRepository->getLastAnswersForSkill($user,$skill, $numberQuestionsInTest);
        $numberOfAnswers = count($answers);
        $numberOfCorrectAnswers = 0;

        $userResponses = array();
        /** @var Answer $answer */
        foreach ($answers as $answer) {
            if ($answer->getOption()->isCorrect()) $numberOfCorrectAnswers++;
            $userResponses[] = array(
                'question' => $answer->getOption()->getQuestion()->getQuestion(),
                'answer' => $answer->getOption()->getText(),
                'correct' => $answer->getOption()->isCorrect()
            );
        }

        $score = array(
            'score' => $numberOfCorrectAnswers,
            'max_score' => $numberOfAnswers,
            'total_questions' => $numberQuestionsInTest
        );
        return View::create(['score'=>$score, 'answers' => $userResponses], 200);
    }

    public function resetAction($id) {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Skill $skill */
        $skill = $this->skillRepository->find($id);
        $answers = $this->answerRepository->getAnswersForSkill($user,$skill);

        foreach ($answers as $answer) {
            $this->entityManager->remove($answer);
        }
        $this->entityManager->flush();

        $score = array(
            'score' => 0,
            'max_score' => 0,
            'total_questions' => 0
        );
        return View::create(['score'=>$score, 'answers' => array()], 200);
    }

    public function completeAction($id, $status) {
        $status = $status ? true : false;
        /** @var User $user */
        $user = $this->getUser();

        /** @var Skill $skill */
        $skill = $this->skillRepository->find($id);

        if (!$skill) {
            return View::create(['error'=>'skill not found'], 404);
        }

        $userSkill = $this->userSkillRepository->findOneBy(array('user'=>$user, 'skill'=>$skill));
        if (!$userSkill) {
            $userSkill = new UserSkill();
            $userSkill->setUser($user)->setSkill($skill);
        }
        $userSkill->setStatus($status);
        $this->entityManager->persist($userSkill);
        $this->entityManager->flush();

        return View::create(['status'=>$status], 200);
    }



}
