<?php

namespace AppBundle\Controller\Api;


use AppBundle\Entity\User;
use AppBundle\Model\Profile\ProfileMapper;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class UserController extends Controller
{

    /**
     * @var ObjectManager $entityManager
     *
     *  @DI\Inject("doctrine.orm.entity_manager"),
     */
    private $entityManager;

    /**
     * @var ProfileMapper $profileMapper
     *
     *  @DI\Inject("app.mapper.profile"),
     */
    private $profileMapper;

    public function profileAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        return View::create(['profile'=>$this->profileMapper->map($user)], 200);
    }

    public function changePasswordAction(Request $request) {
        /** @var User $user */
        $user = $this->getUser();

        $jsonContent = $this->get("request")->getContent();
        $content = json_decode($jsonContent);
        $password = $content->password;

        $user->setPlainPassword($password);
        $user->setChangePassword(false);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return View::create(['profile'=>$this->profileMapper->map($user)], 200);
    }


}
