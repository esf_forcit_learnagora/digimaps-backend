<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Task;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Repository\TaskRepository;


class TaskController extends Controller
{

    /**
     * @var TaskRepository $taskRepository
     *
     * @DI\Inject("digimaps.repository.task")
     */
    private $taskRepository;


    public function indexAction()
    {
        $tasks = $this->taskRepository->findAll();

        return View::create(['results'=>$tasks], 200);
    }


}
