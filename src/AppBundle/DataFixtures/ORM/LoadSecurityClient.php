<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 04.03.16
 * Time: 20:00
 */

namespace AppBundle\DataFixtures\ORM;

use FOS\UserBundle\Doctrine\UserManager;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SecurityBundle\Entity\Client;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User;


class LoadSecurityClient implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $client = new Client();
        $client->setName("frontend");
        $client->setRandomId('18un2muorpr4gwgcw8g088s44k8s8ksgkk4c44o8c44oow8og0');
        $client->setSecret('3j2d0rmww9wk0g8wgoggo4048ks80gkgg484c8gsc0s4sgww88');
        $client->setAllowedGrantTypes(array("password", "refresh_token"));
        $manager->persist($client);
        $manager->flush();
    }

}
