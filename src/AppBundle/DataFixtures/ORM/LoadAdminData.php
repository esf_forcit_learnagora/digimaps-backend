<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 04.03.16
 * Time: 20:00
 */

namespace AppBundle\DataFixtures\ORM;

use FOS\UserBundle\Doctrine\UserManager;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User;


class LoadAdminData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    private $userManager;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->userManager = $this->container->get('fos_user.user_manager');
    }

    public function load(ObjectManager $manager)
    {
        $this->makeUser('bart.kindt@gmail.com','bart','bart','Bart','Kindt');
        $this->makeUser('ennio@forcit.be','ennio','ennio','Ennio','Weckx');
        $this->makeUser('user1@bloovi.me','user1','test');
        $this->makeUser('user2@bloovi.me','user2','test');
        $this->makeUser('user3@bloovi.me','user3','test');
        $this->makeUser('user4@bloovi.me','user4','test');
        $this->makeUser('user5@bloovi.me','user5','test');
        $this->makeUser('annelaura@bloovi.me','annelaura','annelaura');
        $this->makeUser('sofie@bloovi.me','sofie','sofie');
        $this->makeUser('sarah@bloovi.me','sarah','sarah');
        $this->makeUser('camille@bloovi.me','camille','camille');


        /** @var User $userAdmin */
        $userAdmin = $this->userManager->createUser();
        $userAdmin->setUsername('admin');
        $userAdmin->setEmail('admin@test.be');
        $userAdmin->setEnabled(true);
        $userAdmin->setPlainPassword('admin');
        $userAdmin->setSuperAdmin(true);
        $this->userManager->updateUser($userAdmin, true);
    }

    public function makeUser($email, $username, $password, $firstName='', $lastName='') {
        /** @var User $user */
        $user = $this->userManager->createUser();
        $user->setEmail($email);
        $user->setUsername($username);
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $this->userManager->updateUser($user, true);
    }

}
