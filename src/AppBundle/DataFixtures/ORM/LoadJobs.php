<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 04.03.16
 * Time: 20:00
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Format;
use AppBundle\Entity\JobSkill;
use AppBundle\Entity\JobTask;
use AppBundle\Entity\LearningResource;
use AppBundle\Entity\Objective;
use AppBundle\Entity\Topic;
use AppBundle\Entity\Job;
use AppBundle\Entity\Task;
use AppBundle\Entity\Skill;
use AppBundle\Entity\Option;
use AppBundle\Entity\Mcq;
use AppBundle\Model\Csv\Mapper\FormatMapper;
use AppBundle\Model\Csv\Mapper\JobMapper;
use AppBundle\Model\Csv\Mapper\JobTaskMapper;
use AppBundle\Model\Csv\Mapper\ObjectiveMapper;
use AppBundle\Model\Csv\Mapper\QuestionMapper;
use AppBundle\Model\Csv\Mapper\TaskMapper;
use AppBundle\Model\Csv\Mapper\SkillMapper;
use AppBundle\Model\Csv\Mapper\TopicMapper;
use AppBundle\Model\Csv\Mapper\TaskSkillMapper;
use AppBundle\Model\Csv\Model\Format as FormatModel;
use AppBundle\Model\Csv\Model\Job as JobModel;
use AppBundle\Model\Csv\Model\Skill as SkillModel;
use AppBundle\Model\Csv\Model\Task as TaskModel;
use AppBundle\Model\Csv\Model\Topic as TopicModel;
use AppBundle\Model\Csv\Model\Objective as ObjectiveModel;
use AppBundle\Model\Csv\Model\LearningResource as LearningResourceModel;
use AppBundle\Model\Csv\Model\Question as QuestionModel;
use AppBundle\Model\Csv\Model\Option as OptionModel;
use AppBundle\Model\Csv\Model\WeightedSkill;
use AppBundle\Model\Csv\Model\WeightedTask;use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadJobs implements FixtureInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;
    /** @var  ObjectManager */
    private $entityManager;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->entityManager = $manager;
        $directoryPath = $this->container->getParameter('kernel.root_dir') . '/../data/';

        $jobMapper = new JobMapper($directoryPath);
        $jobs = $jobMapper->getJobs();
        $taskMapper = new TaskMapper($directoryPath);
        $tasks = $taskMapper->getTasks();
        $skillMapper = new SkillMapper($directoryPath);
        $skills = $skillMapper->getSkills();

        $taskSkillMapper = new TaskSkillMapper($directoryPath);
        $taskSkillMapper->map($tasks, $skills);

        $jobTaskMapper = new JobTaskMapper($directoryPath);
        $jobTaskMapper->map($jobs,$tasks);

        $topicMapper = new TopicMapper($directoryPath);
        $topics = $topicMapper->map($skills);

        $formatMapper = new FormatMapper($directoryPath);
        $formats = $formatMapper->getFormats();

        $objectiveMapper = new ObjectiveMapper($directoryPath);
        $objectives = $objectiveMapper->map($topics, $formats);

        $questionMapper = new QuestionMapper($directoryPath);
        $questionMapper->map($objectives);

        foreach ($jobs as $jobModel)     $this->persistJobEntity($jobModel);
        foreach ($skills as $skillModel) $this->persistSkillEntity($skillModel);
        foreach ($tasks as $taskModel)   $this->persistTaskEntity($taskModel);
        foreach ($formats as $formatModel) $this->persistFormatEntity($formatModel);
        foreach ($topics as $topicModel)   $this->persistTopicEntity($topicModel);

        /** @var JobModel $jobModel */
        foreach ($jobs as $jobModel) {
            $job = $jobModel->getEntity();
            /** @var WeightedTask $weightedTask */
            foreach ($jobModel->getWeightedTasks() as $weightedTask) {
                $jobTask = new JobTask();
                $jobTask
                    ->setJob($job)
                    ->setTask($weightedTask->getTask()->getEntity())
                    ->setWeight($weightedTask->getWeight());
                $this->entityManager->persist($jobTask);
                $job->addJobTask($jobTask);
            }
            /** @var WeightedSkill $weightedSkill */
            foreach ($jobModel->getWeightedSkills() as $weightedSkill) {
                if ($weightedSkill->getWeight() >= $jobModel->getThreshold()) {
                    $jobSkill = new JobSkill();
                    $jobSkill
                        ->setJob($job)
                        ->setSkill($weightedSkill->getSkill()->getEntity())
                        ->setWeight($weightedSkill->getWeight());
                    $this->entityManager->persist($jobSkill);
                    $job->addJobSkill($jobSkill);
                }
            }
        }
        /** @var SkillModel $skillModel */
        foreach ($skills as $skillModel) {
            $skill = $skillModel->getEntity();
            /** @var TopicModel $topicModel */
            foreach ($skillModel->getTopics() as $topicModel) {
                $skill->addTopic($topicModel->getEntity());
            }
        }

        $this->entityManager->flush();
    }

    private function persistJobEntity(JobModel $jobModel) {
        $job = new Job();
        $job->setName($jobModel->getName());
        $job->setImageName($jobModel->getImageName());
        $job->setDescription($jobModel->getDescription());
        $job->setSource($jobModel->getSource());
        $jobModel->setEntity($job);

        $this->entityManager->persist($job);
    }
    private function persistSkillEntity(SkillModel $skillModel) {
        $skill = new Skill();
        $skill->setName($skillModel->getName());
        $skillModel->setEntity($skill);

        $this->entityManager->persist($skill);
    }
    private function persistTaskEntity(TaskModel $taskModel) {
        $task = new Task();
        $task->setName($taskModel->getName());
        $taskModel->setEntity($task);

        $this->entityManager->persist($task);
    }
    private function persistTopicEntity(TopicModel $topicModel) {
        $topic = new Topic();
        $topic->setName($topicModel->getName());
        $topicModel->setEntity($topic);

        $this->entityManager->persist($topic);

        /** @var ObjectiveModel $objectiveModel */
        foreach ($topicModel->getObjectives() as $objectiveModel) {
            $objective = $this->persistObjectiveEntity($objectiveModel);
            $topic->addObjective($objective);
            $objective->setTopic($topic);
        }
    }
    private function persistFormatEntity(FormatModel $formatModel) {
        $format = new Format();
        $format->setName($formatModel->getName());
        $format->setDescription($formatModel->getDescription());
        $formatModel->setEntity($format);
        $this->entityManager->persist($format);
    }
    private function persistObjectiveEntity(ObjectiveModel $objectiveModel) {
        $objective = new Objective();
        $objective->setName($objectiveModel->getName());
        $this->entityManager->persist($objective);
        /** @var LearningResourceModel $learningResourceModel */
        foreach ($objectiveModel->getLearningResources() as $learningResourceModel) {
            $learningResource = $this->persistLearningResourceEntity($learningResourceModel);
            $objective->addLearningResource($learningResource);
        }
        /** @var QuestionModel $questionModel */
        foreach ($objectiveModel->getQuestions() as $questionModel) {
            $question = $this->persistQuestionEntity($questionModel);
            $objective->addQuestion($question);
        }
        return $objective;
    }
    private function persistLearningResourceEntity(LearningResourceModel $learningResourceModel) {
        $learningResource = new LearningResource();
        $learningResource->setName("name");
        $learningResource->setDescription($learningResourceModel->getDescription());
        $learningResource->setIntroduction($learningResourceModel->getIntro());
        $learningResource->setUrl($learningResourceModel->getUrl());
        $learningResource->setFormat($learningResourceModel->getFormat()->getEntity());
        $this->entityManager->persist($learningResource);
        return $learningResource;
    }
    private function persistQuestionEntity(QuestionModel $questionModel){
        $question = new Mcq();
        $question->setName("");
        $question->setQuestion($questionModel->getQuestion());
        $this->entityManager->persist($question);
        /** @var OptionModel $optionModel */
        foreach ($questionModel->getOptions() as $optionModel) {
            $option = $this->persistOptionEntity($optionModel);
            $question->addOption($option);
            $option->setQuestion($question);
        }
        return $question;
    }
    private function persistOptionEntity(OptionModel $optionModel) {
        $option = new Option();
        $option->setText($optionModel->getText());
        $option->setIsCorrect($optionModel->isCorrect());
        $this->entityManager->persist($option);
        return $option;
    }


}
