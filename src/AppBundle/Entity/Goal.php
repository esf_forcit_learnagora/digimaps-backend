<?php
namespace AppBundle\Entity;

use JMS\Serializer\Annotation as Serializer;

/**
 * Goal
 *
 * @Serializer\ExclusionPolicy("all")
 */

class Goal{

    /** @var int
     *  @Serializer\Expose
     */
    private $id;
    /** @var User */
    private $user;
    /** @var Job
     *  @Serializer\Expose
     */
    private $job;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }


    /**
     * @return User
     */
    public function getUser()  {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Job
     */
    public function getJob() {
        return $this->job;
    }

    /**
     * @param Job $job
     * @return $this
     */
    public function setJob($job) {
        $this->job = $job;
        return $this;
    }


} 