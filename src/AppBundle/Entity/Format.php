<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class Format {

    /** @var int
     *  @Serializer\Expose
     */
    private $id;

    /** @var string
     *  @Serializer\Expose
     */
    private $name;

    /** @var string
     *  @Serializer\Expose
     */
    private $description;


    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return LearningResource
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

} 