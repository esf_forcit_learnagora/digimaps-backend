<?php
namespace AppBundle\Entity;
use JMS\Serializer\Annotation as Serializer;

/**
 * Task
 *
 * @Serializer\ExclusionPolicy("all")
 */

class Task extends BaseEntity{

    private $jobTasks = array();

    /**
     * @return array
     */
    public function getJobTasks() {
        return $this->jobTasks;
    }

    /**
     * @param JobTask $jobTask
     * @return $this
     */
    public function addJobTask($jobTask) {
        $this->jobTasks[] = $jobTask;
        return $this;
    }
} 