<?php
namespace AppBundle\Entity;

class EntityStatus{

    /** @var int */
    private $id;

    /** @var int */
    private $state;

    /** @var User */
    private $user;

    /** @var BaseEntity $entity */
    private $entity;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getState() {
        return $this->state;
    }

    /**
     * @param int $state
     * @return $this
     */
    public function setState($state) {
        $this->state = $state;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()  {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    /**
     * @return BaseEntity
     */
    public function getEntity() {
        return $this->entity;
    }

    /**
     * @param BaseEntity $entity
     * @return $this
     */
    public function setEntity($entity) {
        $this->$entity = $entity;
        return $this;
    }


} 