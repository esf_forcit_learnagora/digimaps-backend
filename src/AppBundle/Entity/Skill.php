<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * Skill
 *
 * @Serializer\ExclusionPolicy("all")
 */

class Skill extends BaseEntity{


    /** @var Collection */
    private $jobSkills;

    /** @var Collection
     *  @Serializer\Expose
     */
    private $topics;


    /**
     * @return Collection
     */
    public function getJobSkills() {
        return $this->jobSkills;
    }

    /**
     * @param JobSkill $jobSkill
     * @return $this
     */
    public function addJobSkill($jobSkill) {
        $this->jobSkills[] = $jobSkill;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getTopics() {
        return $this->topics;
    }

    /**
     * @param Topic $topic
     * @return $this
     */
    public function addTopic($topic) {
        $this->topics[] = $topic;
        return $this;
    }



} 