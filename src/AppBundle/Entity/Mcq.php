<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * Mcq
 *
 * @Serializer\ExclusionPolicy("all")
 */

class Mcq extends Question{

    /** @var Collection
     *  @Serializer\Expose
     */
    private $options = array();



    /**
     * @return Collection
     */
    public function getOptions() {
        return $this->options;
    }

    /**
     * @param Option $option
     * @return $this
     */
    public function addOption($option) {
        $this->options[] = $option;
        return $this;
    }

} 