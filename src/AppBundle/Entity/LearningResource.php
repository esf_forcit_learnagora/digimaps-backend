<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class LearningResource {

    /** @var int
     *  @Serializer\Expose
     */
    private $id;

    /** @var string
     *  @Serializer\Expose
     */
    private $name;

    /** @var string
     *  @Serializer\Expose
     */
    private $description;

    /** @var Format
     *  @Serializer\Expose
     */
    private $format;

    /** @var string
     *  @Serializer\Expose
     */
    private $url;

    /** @var string
     *  @Serializer\Expose
     */
    private $introduction;

    /** @var Collection */
    private $objectives;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getIntroduction()
    {
        return $this->introduction;
    }

    /**
     * @param string $introduction
     * @return LearningResource
     */
    public function setIntroduction($introduction)
    {
        $this->introduction = $introduction;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param mixed $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return LearningResource
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return LearningResource
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getObjectives() {
        return $this->objectives;
    }

    /**
     * @param Objective $objective
     * @return LearningResource
     */
    public function addObjective(Objective $objective) {
        $this->objectives[] = $objective;
        return $this;
    }



} 