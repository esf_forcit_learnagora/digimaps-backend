<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * Skill
 *
 * @Serializer\ExclusionPolicy("all")
 */

class Objective extends BaseEntity{

    /** @var Topic $topic */
    private $topic;

    /** @var Collection */
    private $questions;

    /** @var Collection
     *  @Serializer\Expose
     */
    private $learningResources;

    /**
     * @return Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param Topic $topic
     * @return $this
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
        return $this;
    }


    /**
     * @return Collection
     */
    public function getQuestions() {
        return $this->questions;
    }

    /**
     * @param Question $question
     * @return $this
     */
    public function addQuestion(Question $question) {
        $this->questions[] = $question;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getLearningResources()
    {
        return $this->learningResources;
    }

    /**
     * @param LearningResource $learningResource
     * @return $this
     */
    public function addLearningResource($learningResource)
    {
        $this->learningResources[] = $learningResource;
        return $this;
    }


} 