<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * Job
 *
 * @Serializer\ExclusionPolicy("all")
 */

class JobSkill{

    /** @var integer **/
    private $id;

    /** @var Job
     *  @Serializer\Expose
     */
    private $job;

    /** @var Skill
     *  @Serializer\Expose
     */
    private $skill;

    /** @var integer
     *  @Serializer\Expose
     */
    private $weight = 1;


    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return Job
     */
    public function getJob() {
        return $this->job;
    }

    /**
     * @param Job $job
     * @return $this
     */
    public function setJob($job) {
        $this->job = $job;
        return $this;
    }

    /**
     * @return Skill
     */
    public function getSkill() {
        return $this->skill;
    }

    /**
     * @param Skill $skill
     * @return $this
     */
    public function setSkill($skill) {
        $this->skill = $skill;
        return $this;
    }

    /**
     * @return integer
     */
    public function getWeight() {
        return $this->weight;
    }

    /**
     * @param integer $weight
     * @return $this
     */
    public function setWeight($weight) {
        $this->weight = $weight;
        return $this;
    }

} 