<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * Job
 *
 * @Serializer\ExclusionPolicy("all")
 */

class JobTask{

    /** @var integer **/
    private $id;

    /** @var Job
     *  @Serializer\Expose
     */
    private $job;

    /** @var Task
     *  @Serializer\Expose
     */
    private $task;

    /** @var integer
     *  @Serializer\Expose
     */
    private $weight = 1;


    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return Job
     */
    public function getJob() {
        return $this->job;
    }

    /**
     * @param Job $job
     * @return $this
     */
    public function setJob($job) {
        $this->job = $job;
        return $this;
    }

    /**
     * @return Task
     */
    public function getTask() {
        return $this->task;
    }

    /**
     * @param Task $task
     * @return $this
     */
    public function setTask($task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return integer
     */
    public function getWeight() {
        return $this->weight;
    }

    /**
     * @param integer $weight
     * @return $this
     */
    public function setWeight($weight) {
        $this->weight = $weight;
        return $this;
    }

} 