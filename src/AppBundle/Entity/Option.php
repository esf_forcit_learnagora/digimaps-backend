<?php
namespace AppBundle\Entity;

use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class Option {

    /** @var int
     *  @Serializer\Expose
     */
    private $id;

    /** @var string
     *  @Serializer\Expose
     */
    private $text;

    /** @var bool */
    private $isCorrect = false;

    /** @var Question */
    private $question;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getText() {
        return $this->text;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText($text) {
        $this->text = $text;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCorrect() {
        return $this->isCorrect;
    }

    /**
     * @param bool $isCorrect
     * @return $this
     */
    public function setIsCorrect($isCorrect) {
        $this->isCorrect = $isCorrect;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion() {
        return $this->question;
    }

    /**
     * @param Question $question
     * @return $this
     */
    public function setQuestion($question) {
        $this->question = $question;
        return $this;
    }
} 