<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * Job
 *
 * @Serializer\ExclusionPolicy("all")
 */

class Job extends BaseEntity{

    /** @var string
     *  @Serializer\Expose
     */
    private $imageName;

    /** @var string
     *  @Serializer\Expose
     */
    private $description;

    /** @var string
     *  @Serializer\Expose
     */
    private $source;


    /** @var Collection
     *  @Serializer\Expose
     */
    private $jobTasks = array();

    /** @var Collection
     *  @Serializer\Expose
     */
    private $jobSkills = array();

    /**
     * @return string
     */
    public function getImageName() {
        return $this->imageName;

    }

    /**
     * @param string $imageName
     * @return Job
     */
    public function setImageName($imageName) {
        $this->imageName = $imageName;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource() {
        return $this->source;
    }

    /**
     * @param string $source
     * @return $this
     */
    public function setSource($source) {
        $this->source = $source;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getJobTasks() {
        return $this->jobTasks;
    }

    /**
     * @param JobTask $JobTask
     * @return Job
     */
    public function addJobTask($JobTask) {
        $this->jobTasks[] = $JobTask;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getJobSkills() {
        return $this->jobSkills;
    }

    /**
     * @param JobSkill $jobSkill
     * @return $this
     */
    public function addJobSkill($jobSkill) {
        $this->jobSkills[] = $jobSkill;
        return $this;
    }
} 