<?php
namespace AppBundle\Entity;

use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class Answer {

    /** @var int
     */
    private $id;

    /** @var \Datetime
     */
    private $date;

    /** @var User */
    private $user;

    /** @var Option */
    private $option;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return \Datetime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * @param \Datetime $date
     * @return $this
     */
    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Option
     */
    public function getOption() {
        return $this->option;
    }

    /**
     * @param Option $option
     * @return $this
     */
    public function setOption($option){
        $this->option = $option;
        return $this;
    }
} 