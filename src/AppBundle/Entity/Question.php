<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class Question {

    /** @var int
     *  @Serializer\Expose
     */
    private $id;

    /** @var string
     *  @Serializer\Expose
     */
    private $name;

    /** @var string
     *  @Serializer\Expose
     */
    private $question;

    /** @var Collection */
    private $objectives;
    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Task
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion() {
        return $this->question;
    }

    /**
     * @param string $question
     * @return $this
     */
    public function setQuestion($question) {
        $this->question = $question;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getObjectives() {
        return $this->objectives;
    }

    /**
     * @param Objective $objective
     * @return $this
     */
    public function addObjective(Objective $objective) {
        $this->objectives[] = $objective;
        return $this;
    }
} 