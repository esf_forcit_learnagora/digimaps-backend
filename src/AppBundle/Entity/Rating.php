<?php
namespace AppBundle\Entity;

class Rating {

    /** @var int */
    private $id;

    /** @var User */
    private $user;

    /** @var Job */
    private $job;

    /** @var Task */
    private $task;

    /** @var int */
    private $rating;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Rating
     */
    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Job
     */
    public function getJob() {
        return $this->job;
    }

    /**
     * @param Job $job
     * @return Rating
     */
    public function setJob($job) {
        $this->job = $job;
        return $this;
    }

    /**
     * @return Task
     */
    public function getTask() {
        return $this->task;
    }

    /**
     * @param Task $task
     * @return Rating
     */
    public function setTask($task) {
        $this->task = $task;
        return $this;
    }

    /**
     * @return int
     */
    public function getRating() {
        return $this->rating;
    }

    /**
     * @param int $rating
     * @return Rating
     */
    public function setRating($rating) {
        $this->rating = $rating;
        return $this;
    }
} 