<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * Topic
 *
 * @Serializer\ExclusionPolicy("all")
 */

class Topic extends BaseEntity{

    /** @var Skill $skill */
    private $skills;

    /** @var Collection
     *  @Serializer\Expose
     */
    private $objectives;

    /**
     * @return Collection
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param Skill $skill
     * @return $this
     */
    public function addSkill($skill)
    {
        $this->skills[] = $skill;
        return $this;
    }


    /**
     * @return Collection
     */
    public function getObjectives() {
        return $this->objectives;
    }

    /**
     * @param Objective $objective
     * @return $this
     */
    public function addObjective(Objective $objective) {
        $this->objectives[] = $objective;
        return $this;
    }

} 