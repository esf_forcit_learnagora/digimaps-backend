<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Job;
use AppBundle\Entity\Objective;
use AppBundle\Entity\Question;
use AppBundle\Entity\Skill;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class AnswerRepository extends EntityRepository
{

    public function getAnswersForJob(User $user, Job $job) {
        $query = $this->createQueryBuilder('a')
            ->innerJoin('a.option', 'o')
            ->innerJoin('o.question', 'q')
            ->innerJoin('q.objectives', 'ob')
            ->innerJoin('ob.topic', 't')
            ->innerJoin('t.skills', 's')
            ->innerJoin('s.jobSkills', 'js')
            ->innerJoin('js.job', 'j')
            ->where('a.user = :userId')
            ->andWhere('j.id = :jobId')
            ->getQuery()
            ->setParameters(array(
                'userId'           => $user->getId(),
                'jobId'           => $job->getId(),
            ));

        return $query->getResult();
    }

    public function getAnswersForSkill(User $user, Skill $skill) {
        $query = $this->createQueryBuilder('a')
            ->innerJoin('a.option', 'o')
            ->innerJoin('o.question', 'q')
            ->innerJoin('q.objectives', 'ob')
            ->innerJoin('ob.topic', 't')
            ->innerJoin('t.skills', 's')
            ->where('a.user = :userId')
            ->andWhere('s.id = :skillId')
            ->getQuery()
            ->setParameters(array(
                'userId'           => $user->getId(),
                'skillId'           => $skill->getId(),
            ));

        return $query->getResult();
    }

    public function getLastAnswersForSkill(User $user, Skill $skill, $limit) {
        $query = $this->createQueryBuilder('a')
            ->innerJoin('a.option', 'o')
            ->innerJoin('o.question', 'q')
            ->innerJoin('q.objectives', 'ob')
            ->innerJoin('ob.topic', 't')
            ->innerJoin('t.skills', 's')
            ->where('a.user = :userId')
            ->andWhere('s.id = :skillId')
            ->orderBy('a.date', 'DESC')
            ->setMaxResults( $limit )
            ->getQuery()
            ->setParameters(array(
                'userId'           => $user->getId(),
                'skillId'           => $skill->getId(),
            ));

        return $query->getResult();
    }

    public function getAnswersForTopic(User $user, Topic $topic) {
        $query = $this->createQueryBuilder('a')
            ->innerJoin('a.option', 'o')
            ->innerJoin('o.question', 'q')
            ->innerJoin('q.objectives', 'ob')
            ->innerJoin('ob.topic', 't')
            ->where('a.user = :userId')
            ->andWhere('t.id = :topicId')
            ->getQuery()
            ->setParameters(array(
                'userId'           => $user->getId(),
                'topicId'           => $topic->getId(),
            ));

        return $query->getResult();
    }

    public function getAnswersForObjective(User $user, Objective $objective) {
        $query = $this->createQueryBuilder('a')
            ->innerJoin('a.option', 'o')
            ->innerJoin('o.question', 'q')
            ->innerJoin('q.objectives', 'ob')
            ->where('a.user = :userId')
            ->andWhere('ob.id = :objectiveId')
            ->getQuery()
            ->setParameters(array(
                'userId'           => $user->getId(),
                'objectiveId'           => $objective->getId(),
            ));

        return $query->getResult();
    }
    public function getAnswerFor(User $user, Question $question) {
        $query = $this->createQueryBuilder('a')
            ->innerJoin('a.option', 'o')
            ->where('a.user = :userId')
            ->andWhere('o.question = :questionId')
            ->getQuery()
            ->setParameters(array(
                'userId'           => $user->getId(),
                'questionId'           => $question->getId(),
            ));
        return $query->getOneOrNullResult();
    }
}
