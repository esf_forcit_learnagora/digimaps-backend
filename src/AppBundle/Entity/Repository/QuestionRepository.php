<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Job;
use AppBundle\Entity\Objective;
use AppBundle\Entity\Skill;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class QuestionRepository extends EntityRepository
{

    public function findForJob(Job $job) {
        $query = $this->createQueryBuilder('q')
            ->innerJoin('q.objectives', 'o')
            ->innerJoin('o.topic', 't')
            ->innerJoin('t.skills', 's')
            ->innerJoin('s.jobSkills', 'js')
            ->innerJoin('js.job', 'j')
            ->andWhere('j.id = :jobId')
            ->getQuery()
            ->setParameters(array(
                'jobId'           => $job->getId(),
            ));

        return $query->getResult();
    }

    public function findForSkill(Skill $skill) {
        $query = $this->createQueryBuilder('q')
            ->innerJoin('q.objectives', 'o')
            ->innerJoin('o.topic', 't')
            ->innerJoin('t.skills', 's')
            ->andWhere('s.id = :skillId')
            ->getQuery()
            ->setParameters(array(
                'skillId'           => $skill->getId(),
            ));

        return $query->getResult();
    }


    public function findForTopic(Topic $topic) {
        $query = $this->createQueryBuilder('q')
            ->innerJoin('q.objectives', 'o')
            ->innerJoin('o.topic', 't')
            ->andWhere('t.id = :topicId')
            ->getQuery()
            ->setParameters(array(
                'topicId'           => $topic->getId(),
            ));

        return $query->getResult();
    }

    public function findForObjective(Objective $objective) {
        $query = $this->createQueryBuilder('q')
            ->innerJoin('q.objectives', 'o')
            ->andWhere('o.id = :objectiveId')
            ->getQuery()
            ->setParameters(array(
                'objectiveId'           => $objective->getId(),
            ));

        return $query->getResult();
    }

}
