<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Entity\User as BaseUser;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;

    /** @var string
     *  @Serializer\Expose
     */
    protected $firstName;

    /** @var string
     * @Serializer\Expose
     */
    protected $lastName;

    /** @var bool
     * @Serializer\Expose
     */
    protected $changePassword = false;

    /**
     * @var Collection
     */
    protected $goals = array();

    /**
     * @var Collection
     */
    protected $entityStates = array();

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getChangePassword()
    {
        return $this->changePassword;
    }

    /**
     * @param boolean $changePassword
     * @return $this
     */
    public function setChangePassword($changePassword)
    {
        $this->changePassword = $changePassword;
        return $this;
    }





    public function getGoals() {
        return $this->goals;
    }

    public function getEntityStates() {
        return $this->entityStates;
    }
}
