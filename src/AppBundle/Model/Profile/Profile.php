<?php
namespace AppBundle\Model\Profile;

use AppBundle\Entity\Job;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class Profile {

    /** @var int
     *  @Serializer\Expose
     */
    private $id;

    /** @var string
     *  @Serializer\Expose
     */
    private $username = "test";

    /** @var string
     *  @Serializer\Expose
     */
    private $firstname = "test";

    /** @var string
     *  @Serializer\Expose
     */
    private $lastname = "test";

    /** @var string
     *  @Serializer\Expose
     */
    private $email = "eee";

    /** @var bool
     *  @Serializer\Expose
     */
    private $changePassword = false;

    /** @var Collection
     *  @Serializer\Expose
     */
    private $goals = array();

    /** @var Collection
     *  @Serializer\Expose
     */
    private $scores = array();


    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param Collection $goals
     */
    public function setGoals($goals)
    {
        $this->goals = $goals;
    }

    public function setScores($scores)
    {
        $this->scores = $scores;
    }

    public function changePassword()
    {
        return $this->changePassword;
    }

    public function setChangePassword($changePassword)
    {
        $this->changePassword = $changePassword;
    }

} 