<?php
namespace AppBundle\Model\Profile;

use AppBundle\Entity\Job;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

class Score {

    /** @var int
     */
    private $id;

    /** @var int
     */
    private $score = 0;

    /** @var int
     */
    private $maxScore = 0;

    /** @var int
     */
    private $totalQuestions = 0;

    /** @var int
     */
    private $state = 0;

    public function setid($id) {
        $this->id = $id;
    }
    public function setScore($score) {
        $this->score = $score;
    }
    public function setMaxScore($maxScore) {
        $this->maxScore = $maxScore;
    }
    public function setTotalQuestions($totalQuestions) {
        $this->totalQuestions = $totalQuestions;
    }
    public function setState($state) {
        $this->state = $state;
    }
} 