<?php
namespace AppBundle\Model\Profile;

use AppBundle\Entity\Answer;
use AppBundle\Entity\EntityStatus;
use AppBundle\Entity\Goal;
use AppBundle\Entity\Objective;
use AppBundle\Entity\Repository\AnswerRepository;
use AppBundle\Entity\Repository\EntityStatusRepository;
use AppBundle\Entity\Repository\QuestionRepository;
use AppBundle\Entity\Repository\UserSkillRepository;
use AppBundle\Entity\Skill;
use AppBundle\Entity\Topic;
use AppBundle\Entity\UserSkill;
use JMS\DiExtraBundle\Annotation as DI;
use AppBundle\Entity\User;

/**
 * @DI\Service("app.mapper.profile")
 */
class ProfileMapper {

    private $answerRepository;
    private $questionRepository;
    private $entityStatusRepository;
    /** @var UserSkillRepository */
    private $userSkillRepository;


    /**
     * Constructor.
     *
     * @param AnswerRepository $answerRepository
     * @param QuestionRepository $questionRepository
     * @param UserSkillRepository $userSkillRepository
     *
     * @DI\InjectParams({
     *  "answerRepository" = @DI\Inject("digimaps.repository.answer"),
     *  "questionRepository" = @DI\Inject("digimaps.repository.question"),
     *  "userSkillRepository" = @DI\Inject("digimaps.repository.user_skill")
     * })
     */
    public function __construct(AnswerRepository $answerRepository, QuestionRepository $questionRepository, UserSkillRepository $userSkillRepository) {
        $this->answerRepository = $answerRepository;
        $this->questionRepository = $questionRepository;
        $this->userSkillRepository = $userSkillRepository;
    }

    /**
     * @param User $user
     * @return Profile
     */
    public function map($user) {
        $entityStates = $user->getEntityStates();
        /** @var EntityStatus $entityState */
        foreach ($entityStates as $entityState) {
            $states[$entityState->getEntity()->getId()] = $entityState->getState();
        }

        $profile = new Profile();

        $profile->setId($user->getId());
        $profile->setUsername($user->getUsername());
        $profile->setFirstname($user->getFirstName());
        $profile->setLastname($user->getLastName());
        $profile->setEmail($user->getEmail());
        $profile->setGoals($user->getGoals());
        $profile->setChangePassword($user->getChangePassword());

        $scores = array();
        /** @var Goal $goal */
        foreach ($user->getGoals() as $goal) {
            $job = $goal->getJob();
            $entityId = $job->getId();
            if (!isset($scores[$entityId])) {
                $answers = $this->answerRepository->getAnswersForJob($user, $job);
                $numberOfCorrectAnswers = 0;
                /** @var Answer $answer */
                foreach ($answers as $answer) {
                    if ($answer->getOption()->isCorrect()) $numberOfCorrectAnswers++;
                }
                $score = new Score();
                $score->setid($entityId);
                $score->setScore($numberOfCorrectAnswers);
                $score->setMaxScore(count($answers));
                $score->setTotalQuestions(count($this->questionRepository->findForJob($job)));
                $score->setState(0);
                $scores[$entityId] = $score;
            }

            foreach ($job->getJobSkills() as $jobSkill) {
                /** @var Skill $skill */
                $skill = $jobSkill->getSkill();
                $entityId = $skill->getId();
                if (!isset($scores[$entityId])) {
                    $answers = $this->answerRepository->getAnswersForSkill($user, $skill);
                    $numberOfCorrectAnswers = 0;
                    foreach ($answers as $answer) {
                        if ($answer->getOption()->isCorrect()) $numberOfCorrectAnswers++;
                    }
                    $score = new Score();
                    $score->setid($entityId);
                    $score->setScore($numberOfCorrectAnswers);
                    $score->setMaxScore(count($answers));
                    $score->setTotalQuestions(count($this->questionRepository->findForSkill($skill)));
                    /** @var UserSkill $userSkill */
                    $userSkill = $this->userSkillRepository->findOneBy(array('user'=>$user, 'skill'=>$skill));
                    $score->setState($userSkill ? $userSkill->getStatus() : 0);
                    $scores[$entityId] = $score;
                }

                /** @var Topic $topic */
                foreach ($skill->getTopics() as $topic) {
                    $entityId = $topic->getId();
                    if (!isset($scores[$entityId])) {
                        $answers = $this->answerRepository->getAnswersForTopic($user, $topic);
                        $numberOfCorrectAnswers = 0;
                        foreach ($answers as $answer) {
                            if ($answer->getOption()->isCorrect()) $numberOfCorrectAnswers++;
                        }
                        $score = new Score();
                        $score->setid($entityId);
                        $score->setScore($numberOfCorrectAnswers);
                        $score->setMaxScore(count($answers));
                        $score->setTotalQuestions(count($this->questionRepository->findForTopic($topic)));
                        $score->setState(0);
                        $scores[$entityId] = $score;
                    }

                    /** @var Objective $objective */
                    foreach ($topic->getObjectives() as $objective) {
                        $entityId = $objective->getId();
                        if (!isset($scores[$entityId])) {
                            $answers = $this->answerRepository->getAnswersForObjective($user, $objective);
                            $numberOfCorrectAnswers = 0;
                            foreach ($answers as $answer) {
                                if ($answer->getOption()->isCorrect()) $numberOfCorrectAnswers++;
                            }
                            $score = new Score();
                            $score->setid($entityId);
                            $score->setScore($numberOfCorrectAnswers);
                            $score->setMaxScore(count($answers));
                            $score->setTotalQuestions(count($this->questionRepository->findForObjective($objective)));
                            $score->setState(0);
                            $scores[$entityId] = $score;
                        }
                    }
                }
            }
        }

        $profile->setScores($scores);
        return $profile;
    }

} 