<?php
namespace AppBundle\Model\Csv\Model;



class Question {

    /** @var string */
    private $question = "";
    /** @var array */
    private $options = array();

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param Option $option
     */
    public function addOption($option)
    {
        $this->options[] = $option;
    }

} 