<?php
namespace AppBundle\Model\Csv\Model;

use AppBundle\Entity\Skill as SkillEntity;

class Skill {

    /** @var integer */
    private $id;
    /** @var string  */
    private $name = "";
    /** @var array */
    private $topics = array();

    /** @var SkillEntity */
    private $entity = null;

    /**
     * @return SkillEntity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param SkillEntity $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * @param Topic $topic
     */
    public function addTopic($topic)
    {
        $this->topics[] = $topic;
    }


} 