<?php
namespace AppBundle\Model\Csv\Model;

use AppBundle\Entity\Task as TaskEntity;

class Task {

    /** @var integer */
    private $id;
    /** @var string  */
    private $name = "";
    /** @var array  */
    private $weightedSkills = array();
    /** @var TaskEntity */
    private $entity = null;

    /**
     * @return TaskEntity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param TaskEntity $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getWeightedSkills()
    {
        return $this->weightedSkills;
    }

    /**
     * @param WeightedSkill $weightedSkill
     */
    public function addWeightedSkill($weightedSkill)
    {
        $this->weightedSkills[] = $weightedSkill;
    }


} 