<?php
namespace AppBundle\Model\Csv\Model;

use AppBundle\Entity\Topic as TopicEntity;

class Topic {

    /** @var integer */
    private $id;
    /** @var string  */
    private $name = "";
    /** @var string  */
    private $description = "";
    /** @var array */
    private $objectives = array();
    /** @var TopicEntity */
    private $entity = null;

    /**
     * @return TopicEntity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param TopicEntity $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getObjectives()
    {
        return $this->objectives;
    }

    /**
     * @param Objective $objective
     */
    public function addObjective($objective)
    {
        $this->objectives[] = $objective;
    }


} 