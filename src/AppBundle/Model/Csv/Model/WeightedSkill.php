<?php
namespace AppBundle\Model\Csv\Model;

class WeightedSkill {

    /** @var integer  */
    private $weight = 0;
    /** @var Skill */
    private $skill = null;

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return Skill
     */
    public function getSkill()
    {
        return $this->skill;
    }

    /**
     * @param Skill $skill
     */
    public function setSkill($skill)
    {
        $this->skill = $skill;
    }

} 