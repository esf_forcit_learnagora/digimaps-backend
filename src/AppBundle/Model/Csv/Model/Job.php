<?php
namespace AppBundle\Model\Csv\Model;

use AppBundle\Entity\Job as JobEntity;

class Job {

    /** @var integer */
    private $id;
    /** @var string  */
    private $name = "";
    /** @var string  */
    private $imageName = "";
    /** @var string  */
    private $description = "";
    /** @var string  */
    private $source = "";
    /** @var string  */
    private $threshold = 0;

    /** @var array  */
    private $weightedTasks = array();
    /** @var array  */
    private $weightedSkills = array();

    /** @var JobEntity */
    private $entity = null;

    /**
     * @return JobEntity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param JobEntity $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getThreshold()
    {
        return $this->threshold;
    }

    /**
     * @param string $threshold
     */
    public function setThreshold($threshold)
    {
        $this->threshold = $threshold;
    }

    /**
     * @return array
     */
    public function getWeightedTasks()
    {
        return $this->weightedTasks;
    }

    /**
     * @param WeightedTask $weightedTask
     */
    public function addWeightedTask($weightedTask)
    {
        $this->weightedTasks[] = $weightedTask;
    }

    /**
     * @return array
     */
    public function getWeightedSkills()
    {
        return $this->weightedSkills;
    }

    /**
     * @param WeightedSkill $weightedSkill
     */
    public function addWeightedSkill($weightedSkill)
    {
        $this->weightedSkills[] = $weightedSkill;
    }

    public function updateWeightedSkills() {
        /** @var WeightedTask $weightedTask */
        foreach ($this->weightedTasks as $weightedTask) {
            $taskWeight = $weightedTask->getWeight();
            /** @var WeightedSkill $weightedTaskSkill */
            foreach ($weightedTask->getTask()->getWeightedSkills() as $weightedTaskSkill) {
                $skillWeight = $weightedTaskSkill->getWeight();
                $totalWeight = $taskWeight * $skillWeight;
                $weightedSkill = $this->getWeightedSkill($weightedTaskSkill->getSkill()->getId());
                if (is_null($weightedSkill)) {
                    $weightedSkill = new WeightedSkill();
                    $weightedSkill->setSkill($weightedTaskSkill->getSkill());
                    $this->addWeightedSkill($weightedSkill);
                }
                $weightedSkill->setWeight($weightedSkill->getWeight()+$totalWeight);
            }
        }
    }

    /**
     * @param int $skillId
     * @return WeightedSkill|null
     */
    public function getWeightedSkill($skillId) {
        /** @var WeightedSkill $weightedSkill */
        foreach ($this->weightedSkills as $weightedSkill) {
            if ($weightedSkill->getSkill()->getId() == $skillId) {
                return $weightedSkill;
            }
        }
        return null;
    }

} 