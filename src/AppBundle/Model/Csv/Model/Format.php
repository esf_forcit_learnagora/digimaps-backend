<?php
namespace AppBundle\Model\Csv\Model;

use AppBundle\Entity\Format as FormatEntity;

class Format {

    private $name;
    private $description = "";
    /** @var FormatEntity */
    private $entity = null;

    /**
     * @return FormatEntity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param FormatEntity $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }



} 