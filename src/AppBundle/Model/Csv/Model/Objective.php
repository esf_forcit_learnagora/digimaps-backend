<?php
namespace AppBundle\Model\Csv\Model;



class Objective {

    /** @var integer */
    private $id;
    /** @var string  */
    private $name = "";
    /** @var array */
    private $learningResources = array();
    /** @var array */
    private $questions = array();

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getLearningResources()
    {
        return $this->learningResources;
    }

    /**
     * @param LearningResource $learningResource
     */
    public function addLearningResource($learningResource)
    {
        $this->learningResources[] = $learningResource;
    }

    /**
     * @return array
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param Question $question
     */
    public function addQuestion($question)
    {
        $this->questions[] = $question;
    }

} 