<?php
namespace AppBundle\Model\Csv\Model;

class WeightedTask {

    /** @var integer  */
    private $weight = 0;
    /** @var Task */
    private $task = null;

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param Task $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

} 