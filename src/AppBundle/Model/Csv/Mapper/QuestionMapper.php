<?php
namespace AppBundle\Model\Csv\Mapper;

use AppBundle\Model\Csv\Model\LearningResource;
use AppBundle\Model\Csv\Model\Option;
use AppBundle\Model\Csv\Model\Question;
use AppBundle\Model\Csv\Model\Topic;
use AppBundle\Model\Csv\Model\Objective;

class QuestionMapper extends Mapper {

    const FILENAME = 'activities.csv';

    const COLUMN_TOPIC_ID = 0;
    const COLUMN_OBJECTIVE_ID = 1;
    const COLUMN_QUESTION_ID = 2;
    const COLUMN_QUESTION_TYPE = 3;
    const COLUMN_QUESTION_QUESTION = 4;
    const COLUMN_OPTION_TEXT = 5;
    const COLUMN_OPTION_CORRECT = 6;


    public function getFilename() {
        return self::FILENAME;
    }


    public function map($objectives) {
        $lines = $this->getlines();
        /** @var Question $currentQuestion */
        $currentQuestion = null;
        foreach ($lines as $index=>$line) {
            $objective = $this->getObjective($objectives, $line);
            $question = $this->getQuestion($line);
            if ($objective && $question) {
                $objective->addQuestion($question);
                $currentQuestion = $question;
            }

            $option = $this->getOption($line);
            if ($currentQuestion && $option) {
                $currentQuestion->addOption($option);
            }
        }
    }

    private function getObjective($objectives, $line) {
        if ($line[self::COLUMN_OBJECTIVE_ID] == "")
            return null;

        $objectiveId = $line[self::COLUMN_OBJECTIVE_ID];
        /** @var Objective $objective */
        foreach ($objectives as $objective) {
            if ($objective->getId() == $objectiveId)
                return $objective;
        }
        return null;
    }

    private function getQuestion($line) {
        if ($line[self::COLUMN_QUESTION_ID] == "")
            return null;

        $question = new Question();
        $question->setQuestion($line[self::COLUMN_QUESTION_QUESTION]);
        return $question;
    }
    private function getOption($line) {
        if ($line[self::COLUMN_OPTION_TEXT] == "")
            return null;

        $option = new Option();
        $option->setText($line[self::COLUMN_OPTION_TEXT]);
        $option->setCorrect($line[self::COLUMN_OPTION_CORRECT] != '');
        return $option;
    }
} 