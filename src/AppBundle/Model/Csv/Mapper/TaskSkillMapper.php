<?php
namespace AppBundle\Model\Csv\Mapper;

use AppBundle\Model\Csv\Model\Task as TaskModel;
use AppBundle\Model\Csv\Model\Skill as SkillModel;
use AppBundle\Model\Csv\Model\WeightedSkill;

class TaskSkillMapper extends Mapper {

    const FILENAME = 'task_skill.csv';

    public function getFilename() {
        return self::FILENAME;
    }

    public function map($tasks,$skills) {
        $lines = $this->getlines();
        foreach ($lines as $taskIndex => $line) {
            for ($skillIndex=0; $skillIndex<count($skills); $skillIndex++) {
                /** @var TaskModel $task */
                $task = $tasks[$taskIndex];
                /** @var SkillModel $skill */
                $skill = $skills[$skillIndex];

                $weight = $line[$skillIndex+1];
                if ($weight != '') {
                    $weight = $weight!='x' ? $weight : 1;
                    $weightedSkill = new WeightedSkill();
                    $weightedSkill->setWeight($weight);
                    $weightedSkill->setSkill($skill);

                    $task->addWeightedSkill($weightedSkill);
                }
            }
        }
    }

} 