<?php
namespace AppBundle\Model\Csv\Mapper;

use AppBundle\Model\Csv\Model\Format;
use AppBundle\Model\Csv\Model\LearningResource;
use AppBundle\Model\Csv\Model\Topic;
use AppBundle\Model\Csv\Model\Objective;

class ObjectiveMapper extends Mapper {

    const FILENAME = 'lessons.csv';

    const COLUMN_TOPIC_ID = 0;
    const COLUMN_OBJECTIVE_ID = 1;
    const COLUMN_OBJECTIVE_NAME = 2;
    const COLUMN_LEARNING_RESOURCE_DESCRIPTION = 3;
    const COLUMN_LEARNING_RESOURCE_FORMAT = 4;
    const COLUMN_LEARNING_RESOURCE_URL = 5;
    const COLUMN_LEARNING_RESOURCE_INTRO = 6;

    /** @var array */
    private $topics = array();
    /** @var Topic */
    private $currentTopic = null;

    /** @var array */
    private $objectives = array();

    public function getFilename() {
        return self::FILENAME;
    }

    public function map($topics, $formats) {
        $this->topics = $topics;

        $lines = $this->getlines();
        foreach ($lines as $index=>$line) {
            $this->updateCurrentTopic($line);
            if (!$this->currentTopic)
                continue;

            $objective = $this->getObjective($line);
            if ($objective && !$this->currentTopicHasObjective($objective))
                $this->currentTopic->addObjective($objective);

            $learningResource = $this->getLearningResource($line,$formats);
            if ($objective && $learningResource)
                $objective->addLearningResource($learningResource);
        }
        return $this->objectives;
    }

    private function updateCurrentTopic($line) {
        if ($line[self::COLUMN_TOPIC_ID] != "") {
            $topicId = $line[self::COLUMN_TOPIC_ID];
            /** @var Topic $topic */
            foreach ($this->topics as $topic) {
                if ($topic->getId() == $topicId) {
                    $this->currentTopic = $topic;
                }
            }
        }
    }
    private function currentTopicHasObjective(Objective $objective) {
        /** @var Objective $o */
        foreach ($this->currentTopic->getObjectives() as $o) {
            if ($o->getId() == $objective->getId())
                return true;
        }
        return false;
    }
    private function getObjective($line) {
        if ($line[self::COLUMN_OBJECTIVE_ID] == "")
            return null;

        $objectiveId = $line[self::COLUMN_OBJECTIVE_ID];
        $objective = null;
        /** @var Objective $o */
        foreach ($this->objectives as $o) {
            if ($o->getId() == $objectiveId) {
                $objective = $o;
            }
        }

        if (is_null($objective)) {
            $objective = new Objective();
            $objective->setId($objectiveId);
            $objective->setName($line[self::COLUMN_OBJECTIVE_NAME]);
            $this->objectives[] = $objective;
        }

        return $objective;
    }

    private function getLearningResource($line, $formats) {
        if ($line[self::COLUMN_LEARNING_RESOURCE_DESCRIPTION] == "")
            return null;
        $learningResource = new LearningResource();
        $learningResource->setDescription($line[self::COLUMN_LEARNING_RESOURCE_DESCRIPTION]);
        $learningResource->setUrl($line[self::COLUMN_LEARNING_RESOURCE_URL]);
        $learningResource->setIntro($line[self::COLUMN_LEARNING_RESOURCE_INTRO]);

        echo "looking for format ".$line[self::COLUMN_LEARNING_RESOURCE_FORMAT]."\n";
        /** @var Format $format */
        foreach ($formats as $format) {
            //echo "i have format ".$format->getName()."\n";
            if ($format->getName() == $line[self::COLUMN_LEARNING_RESOURCE_FORMAT]) {
                //echo "found it"."\n";
                $learningResource->setFormat($format);
            }
        }

        return $learningResource;
    }
} 