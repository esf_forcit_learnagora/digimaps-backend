<?php
namespace AppBundle\Model\Csv\Mapper;

use AppBundle\Model\Csv\Model\Skill;
use AppBundle\Model\Csv\Model\Topic;

class TopicMapper extends Mapper {

    const FILENAME = 'topics.csv';

    const COLUMN_ID = 0;
    const COLUMN_NAME = 1;
    const COLUMN_DESCRIPTION = 3;
    const COLUMN_SKILLS_START = 4;

    public function getFilename() {
        return self::FILENAME;
    }

    public function map($skills) {
        $topics = array();
        $lines = $this->getlines();
        foreach ($lines as $index=>$line) {
            if ($line[self::COLUMN_ID]) {
                $topic = new Topic();
                $topic->setId($line[self::COLUMN_ID]);
                $topic->setName($line[self::COLUMN_NAME]);
                $topic->setDescription($line[self::COLUMN_DESCRIPTION]);
                $topics[] = $topic;
                for ($skillIndex=0; $skillIndex<count($skills); $skillIndex++) {
                    $mark = $line[self::COLUMN_SKILLS_START+$skillIndex];
                    if ($mark != '') {
                        /** @var Skill $skill */
                        $skill = $skills[$skillIndex];
                        $skill->addTopic($topic);
                    }
                }
            }
        }
        return $topics;
    }

} 