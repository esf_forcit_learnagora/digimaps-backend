<?php
namespace AppBundle\Model\Csv\Mapper;

use AppBundle\Model\Csv\Model\Format;

class FormatMapper extends Mapper {

    const FILENAME = 'formats.csv';

    const COLUMN_NAME = 0;
    const COLUMN_DESCRIPTION = 1;

    public function getFilename() {
        return self::FILENAME;
    }

    public function getFormats() {
        $formats = array();
        $lines = $this->getlines();
        foreach ($lines as $index=>$line) {
            $format = new Format();
            $format->setName($line[self::COLUMN_NAME]);
            $format->setDescription($line[self::COLUMN_DESCRIPTION]);
            $formats[] = $format;
        }
        return $formats;
    }

} 