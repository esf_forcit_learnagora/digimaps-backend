<?php
namespace AppBundle\Model\Csv\Mapper;

use AppBundle\Model\Csv\Model\Task as TaskModel;

class TaskMapper extends Mapper {

    const FILENAME = 'tasks.csv';

    const COLUMN_NAME = 1;

    public function getFilename() {
        return self::FILENAME;
    }

    public function getTasks() {
        $tasks = array();
        $lines = $this->getlines();
        foreach ($lines as $index=>$line) {
            $task = new TaskModel();
            $task->setId($index);
            $task->setName($line[self::COLUMN_NAME]);
            $tasks[] = $task;
        }
        return $tasks;
    }

} 