<?php
namespace AppBundle\Model\Csv\Mapper;



abstract class Mapper implements MapperInterface {

    private $path = "";

    public function __construct($path) {
        $this->setPath($path);
    }

    public function setPath($path) {
        $this->path = $path;
    }

    public function getLines() {
        $handle = fopen($this->path . $this->getFilename(), "r");
        $lines = array();
        if ($handle) {
            while (($line = fgetcsv($handle, 1000, ",")) !== false) {
                $lines[] = $line;
            }
            fclose($handle);
        }
        return $lines;
    }

} 