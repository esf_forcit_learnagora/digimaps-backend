<?php
namespace AppBundle\Model\Csv\Mapper;

use AppBundle\Model\Csv\Model\Job as JobModel;
use AppBundle\Model\Csv\Model\Task as TaskModel;
use AppBundle\Model\Csv\Model\WeightedTask;

class JobTaskMapper extends Mapper {

    const FILENAME = 'job_task.csv';

    public function getFilename() {
        return self::FILENAME;
    }

    public function map($jobs,$tasks) {
        $lines = $this->getlines();
        foreach ($lines as $taskIndex => $line) {
            for ($jobIndex=0; $jobIndex<count($jobs); $jobIndex++) {
                /** @var JobModel $job */
                $job = $jobs[$jobIndex];
                /** @var TaskModel $task */
                $task = $tasks[$taskIndex];

                $weight = $line[$jobIndex+1];
                if ($weight != '') {
                    $weight = $weight!='x' ? $weight : 1;
                    $weightedTask = new WeightedTask();
                    $weightedTask->setWeight($weight);
                    $weightedTask->setTask($task);

                    $job->addWeightedTask($weightedTask);
                }
            }
        }
        foreach ($jobs as $job) {
            $job->updateWeightedSkills();
        }
    }

} 