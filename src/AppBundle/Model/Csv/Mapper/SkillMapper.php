<?php
namespace AppBundle\Model\Csv\Mapper;

use AppBundle\Model\Csv\Model\Skill as SkillModel;

class SkillMapper extends Mapper {

    const FILENAME = 'skills.csv';

    const COLUMN_NAME = 1;

    public function getFilename() {
        return self::FILENAME;
    }

    public function getSkills() {
        $skills = array();
        $lines = $this->getlines();
        foreach ($lines as $index=>$line) {
            $skill = new SkillModel();
            $skill->setId($index);
            $skill->setName($line[self::COLUMN_NAME]);
            $skills[] = $skill;
        }
        return $skills;
    }

} 