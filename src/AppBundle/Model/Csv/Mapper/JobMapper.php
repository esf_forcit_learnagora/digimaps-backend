<?php
namespace AppBundle\Model\Csv\Mapper;

use AppBundle\Model\Csv\Model\Job as JobModel;

class JobMapper extends Mapper {

    const FILENAME = 'jobs.csv';

    const COLUMN_NAME = 1;
    const COLUMN_IMAGE_NAME = 2;
    const COLUMN_DESCRIPTION = 3;
    const COLUMN_SOURCE = 4;
    const COLUMN_THRESHOLD = 5;

    public function getFilename() {
        return self::FILENAME;
    }

    public function getJobs() {
        $jobs = array();
        $lines = $this->getlines();
        foreach ($lines as $index=>$line) {
            $job = new JobModel();
            $job->setId($index);
            $job->setName($line[self::COLUMN_NAME]);
            $job->setImageName($line[self::COLUMN_IMAGE_NAME]);
            $job->setDescription($line[self::COLUMN_DESCRIPTION]);
            $job->setSource($line[self::COLUMN_SOURCE]);
            $job->setThreshold($line[self::COLUMN_THRESHOLD]);
            $jobs[] = $job;
        }
        return $jobs;
    }

} 